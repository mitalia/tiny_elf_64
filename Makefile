all: tiny_base tiny_elf tiny_elf_over_1 tiny_elf_over_2

tiny_base: tiny_base.o
	ld -s tiny_base.o -o tiny_base
tiny_base.o: tiny_base.s
	nasm -f elf64 tiny_base.s
tiny_elf: tiny_elf.s
	nasm tiny_elf.s -o tiny_elf
	chmod +x tiny_elf
tiny_elf_over_1: tiny_elf_over_1.s
	nasm tiny_elf_over_1.s -o tiny_elf_over_1
	chmod +x tiny_elf_over_1
tiny_elf_over_2: tiny_elf_over_2.s
	nasm tiny_elf_over_2.s -o tiny_elf_over_2
	chmod +x tiny_elf_over_2

clean:
	rm -f tiny_base.o tiny_base tiny_elf tiny_elf_over_1 tiny_elf_over_2
